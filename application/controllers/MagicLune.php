<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class MagicLune extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('PanelModel', 'panel');
        $this->load->model('LightModel', 'light');
        //$this->load->model('FormModel', 'form');

    }

    public function index() {
        $html = $this->load->view('common/pagina', null, true);

        $this->show($html);
    }

    public function light() {
        $data['light'] = $this->light->getLight(); 
        $data['light2'] = $this->light->getLight2(); 
        $html = $this->load->view('componente/light', $data, true);

        $this->show($html);
    }

    public function panel() {
        $data['panel'] = $this->panel->getPanel();       
        $data['panel2'] = $this->panel->getPanel2();
        $data['panel3'] = $this->panel->getPanel3();  
        $data['panel4'] = $this->panel->getPanel4();  
        $data['panel5'] = $this->panel->getPanel5(); 
        $data['panel6'] = $this->panel->getPanel6(); 
        $data['panel7'] = $this->panel->getPanel7(); 
        $data['panel8'] = $this->panel->getPanel8(); 
        $data['panel9'] = $this->panel->getPanel9(); 
        $data['panel10'] = $this->panel->getPanel10(); 
        $data['panel11'] = $this->panel->getPanel11(); 
        $data['panel12'] = $this->panel->getPanel12(); 
         
        $html = $this->load->view('componente/panel', $data, true);
        
        $this->show($html);
    }

    public function form() {
        $html = $this->load->view('componente/form', null, true);

        $this->show($html);
    }

    public function teste() {
        $this->show('<iframe src="'.base_url('test/HelloTest').'" class="w-100 m-0"  style="height:100vh;" allowfullscreen></iframe>', false);
    }
}
