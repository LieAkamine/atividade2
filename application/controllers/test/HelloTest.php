<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/Toast.php');
include_once APPPATH. 'libraries/panel/PanelData.php';
include_once APPPATH. 'libraries/light/LightData.php';

class HelloTest extends Toast{
	
	function __construct() {
		parent::__construct('HelloTest');
	}

	function test_nome_valido() {
		$loader = (object) array(
            "titulo" => 'Panel title',
            "header" => false,
            "footer" => false,
            "conteudo" => '<p class="card-text"> Some quick example text to build on the panel title and make up the bulk of the panels content. </p>             <a class="card-link"> Card link </a>
            <a class="card-link"> Another link </a>',
            "cor" => '',
            "header_titulo" => '',
            "footer_titulo" => ''
        );
		$panel = new PanelData($loader);
		$nome = !empty($panel->titulo()) ? true : false;
		$this->_assert_true($nome, "Erro: Coloque o nome válido");
	}

	function test_campos_n_nulos() {
		$loader = (object) array(
            "imagem" => 'box2.jpg',
            "alt" => 'Teste do teste',
            "classe" => 'opacity'
        );
		$light = new LightData($loader);
		$imagem = !empty($light->imagem()) ? true : false;
		$alt = !empty($light->alt()) ? true : false;
		$classe = !empty($light->classe()) ? true : false;
		$this->_assert_true($imagem, $alt, $classe, "Erro: Campos nulos!"); 
	}


	
}