<header>
<nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
    <a class="navbar-brand" href="<?= base_url("magiclune/index")?>"><img src="<?= base_url("assets/Img/cube.png") ?>" title="Magic Lune Café" height="50px" width="50px"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" 
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <!-- <span class="navbar-toggler-icon"></span> -->
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url("magiclune/light") ?>"> * LightBox </a> <!-- Essa parte está acertado, é o sobre da empresa -->
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url("magiclune/panel") ?>"> * Panel </a> <!-- Essa parte está acertado, é o contato da empresa -->
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url("magiclune/form") ?>"> * Form -> Input Groups </a> <!-- Essa parte está acertado, é o cardápio -->
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url("magiclune/teste") ?>"> * Testes </a> <!-- Essa parte está acertado, é o cardápio -->
          </li>
        </ul>
      </div>
  </nav>
  </header>
  <br/>
  <br/>