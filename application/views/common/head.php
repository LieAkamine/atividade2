<html class="full-height">
    <head>
        <title> ༺ 𝓒𝓸𝓶𝓹𝓸𝓷𝓮𝓷𝓽𝓮𝓼 𝓗𝓣𝓜𝓛 ༻ </title>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <link rel="shortcut icon" type="image/icon" href="<?= base_url("assets/Img/logo3.png") ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-flit=no"/>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.7/css/mdb.min.css" rel="stylesheet"/>
    </head>
    <style>

        a:visited {
            text-decoration: none;
        }

        a:hover {
            color: Blue;
        }

        a:active {
            color: Blue;
        }

        code {
            color: #e83e8c;
            word-break: normal;
        }

        p {
            text-align: center;
            word-break: normal;
        }

        body {
            background: url(" <?= base_url("assets/Img/fundo2.jpg")?>") no-repeat center center fixed; 
            background-size: cover;
        }

        .image_carousel {
            width: inherit;
            height: inherit;
            background-size: cover;
        }

        .view {
        background-size: cover;
        }

        .navbar {
            background: url(" <?= base_url("assets/Img/colors.jpg")?>") no-repeat center center fixed;
            background-size: cover;
        }

        .top-nav-collapse {
            background: url(" <?= base_url("assets/Img/colors.jpg")?>") no-repeat center center fixed;
            background-size: cover;
            width: 100%;
        }

        .card-img-top {
            object-fit: fill;
            max-width: 100%;
            width: 350px;
            height: 350px;
        }

        div.mycontainer {
            background-color:rgba(255,255,255,0.8);
            background-position: center center;
            text-align: center;
        }

        div.container-pag {
            background-color:rgba(255,255,255,0.8);
            background-position: inherit;
            padding: 5 50 5 50; 
            text-align: center;
        }

        div.mytext-center img{
            width: 500px;
            height: 300px;
        }

        /* Para exibir código */
        #p_code {
            background-color: rgba(192,192,192,0.8);
            background-position: inherit;
            padding: 5 50 5 50;
            text-align: left;
        }

        /* Aqui é para o Lightbox */
        img.opacity {
            width: 300px;
            height: 300px;
            opacity: 0.5;
            filter: alpha(opacity = 100); /* IE8 e anteriores */
            zoom: 1; /* Ativa "hasLayout" no IE 7 e anteriores */
            }
            
        img.opacity:hover {
            opacity: 1.0;
            filter: brightness (200%); 
            zoom: 1;
            }

        /* Até aqui */        

        @media only screen and (max-width: 768px) {
            .navbar {
                background: url(" <?= base_url("assets/Img/colors.jpg")?>") no-repeat center center fixed;
                background-size: cover;
            }

            .top-nav-collapse {
                background: url(" <?= base_url("assets/Img/colors.jpg")?>") no-repeat center center fixed;
                background-size: cover;
            }

            .card-img-top {
            object-fit: fill;
            max-width: 100%;
            width: auto;
            height: 350px;
            }
        }

        @media (min-width: 992px) {
            html, body, .view {
                height: 100%;
            }

            .card-img-top {
            object-fit: fill;
            max-width: 100%;
            width: auto;
            height: 350px;
            }
        }
        
        </style>
    <body>