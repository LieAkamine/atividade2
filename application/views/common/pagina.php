<header>
<div class="container-fluid">
  <div class="view">
    <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-1z" data-slide-to="1"></li>
        <li data-target="#carousel-example-1z" data-slide-to="2"></li>
        <li data-target="#carousel-example-1z" data-slide-to="3"></li>
      </ol>
      <div class="carousel-inner" role="listbox">

        <div class="carousel-item active">
          <img class="d-block w-100" src="<?= base_url("assets/Img/img.gif") ?>" alt="First slide">
            <div class="mask rgba-stylish-light flex-center white-text">
              <ul class="animated fadeInUp col-md-12 list-unstyled list-inline">
                <li>
                  <h1 class="font-weight-bold"> Componentes HTML </h1>
                </li>
                <li>
                  <p class="font-weight-bold py-4"> Veja exemplos de como utilizar componentes HTML em seu próprio site! </p>
                </li>
              </ul>
          </div>
        </div>

        <div class="carousel-item">
          <img class="d-block w-100" src="<?= base_url("assets/Img/img2.gif") ?>" alt="Second slide">
            <div class="mask rgba-stylish-light flex-center white-text">
              <ul class="animated fadeInUp col-md-12 list-unstyled list-inline">
                <li>
                  <h1 class="font-weight-bold"> Lightbox </h1>
                </li>
                <li>
                  <p class="font-weight-bold py-4"> Quer dar um efeito extra nas suas imagens? Confira o Lightbox! </p>
                </li>
              </ul>
          </div>
        </div>
 
        <div class="carousel-item">
          <img class="d-block w-100" src="<?= base_url("assets/Img/img3.gif") ?>" alt="Third slide">
          <div class="mask rgba-stylish-light flex-center white-text">
              <ul class="animated fadeInUp col-md-12 list-unstyled list-inline">
                <li>
                  <h1 class="font-weight-bold"> Panel </h1>
                </li>
                <li>
                  <p class="font-weight-bold py-4"> Utilizar um Panel ou Card? Veja como é um Panel e todas as suas funcionalidades! </p>
                </li>
              </ul>
          </div>
        </div>

        <div class="carousel-item">
          <img class="d-block w-100" src="<?= base_url("assets/Img/img4.gif") ?>" alt="slide">
          <div class="mask rgba-stylish-light flex-center white-text">
              <ul class="animated fadeInUp col-md-12 list-unstyled list-inline">
                <li>
                  <h1 class="font-weight-bold"> Form - Input Groups </h1>
                </li>
                <li>
                  <p class="font-weight-bold py-4"> Quer entender como funciona um formulário de dados? Veja aqui! </p>
                </li>
              </ul>
          </div>
        </div>
 
    <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  

    </div>
  </div>
</div>
</header>

<main class="text-center py-2">

</main>