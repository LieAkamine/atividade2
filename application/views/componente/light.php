<div class="clearfix mt-5 mb-6">
    <div class="container">
        <div class="container-pag">
            <div class="col-mt-5 mb-6">
                <h2 class='h2-responsive p-0.5 m-0.5 blue-gradient text-white text-center'>
                    LightBox
                </h2>
            
                <p>
                O efeito de Lightbox é utilizado para dar um toque especial nas imagens do seu site, podendo ser facilmente
                adicionados em uma galeria de fotos, carousel, card e entre outros elementos encontrados no <code> Mdbootstrap </code>
                ou no <code> W3School </code>.
                </p>
                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            <?= $light ?> 
                        </div>
                    </div>

                        </br>

                    <div class="row border rounded-md grey">
                        <pre>
                        <code>
                        &lt;figure&gt;
                            &lt;img src="url" alt="Lightbox" class="opacity"&gt;
                        &lt;/figure&gt;
                                
                        </code>
                        </pre>
                    </div>
                    </br>

                        <div class="row justify-content-center">
                            <div class="col-mt-12">
                                <?= $light2 ?> 
                            </div>
                        </div>
               
                </div>
            </div>
        </div>
    </div>
</div>

