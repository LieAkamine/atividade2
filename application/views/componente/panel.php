<div class="clearfix mt-5 mb-6">
    <div class="container">
        <div class="container-pag">
            <div class="col-mt-5 mb-6">
                <h2 class='h2-responsive p-0.5 m-0.5 blue-gradient text-white text-center'>
                    Conteúdo 
                </h2>

                <a href="#P1"> Painel </a> <br/>
                <a href="#P2"> Títulos, Texto, e Links </a> <br/>
                <a href="#P3"> Cabeçalho e Rodapé </a> <br/>
                <a href="#P4"> Using custom CSS </a> <br/>
                <a href="#P5"> Text alignment </a> <br/>
                <a href="#P6"> Panel styles </a> <br/>
                <a href="#P7"> Border </a> <br/>

                <a class="#P1"><h2 class='h2-responsive p-0.5 m-0.5 blue-gradient text-white text-center'></a>
                    Panel
                </h2>
                
                    <h4><b> Painel Bootstrap </b></h4> 
                    <p> 
                        Os "Painéis" do Bootstrap fornecem um contêiner de conteúdo flexível com várias possíveis modificações.
                        Temos opções como por exemplo, incluir Cabeçalhos e Rodapés, variedade de conteúdos, cores de fundos,
                        entre outros. Os painéis são semelhantes a um "Card", mas não incluem mídia.
                    </p>
                    <hr>

                    <h4><b> Exemplos Básicos </b></h4>
                    <p>
                    Os painéis são construídos com o mínimo de marcação e estilos possíveis e consegue oferecer uma tonelada de
                    controle e personalização, fácil alinhamento e se misturam bem com outros componenetes do Bootstrap.
                    Eles não têm <code> margem </code> predefinidos, portando, use os <code> utilitários de espaçamento </code> conforme necessário.
                    </p>
                    
                    <p>
                    Abaixo temos um exemplo de um <code> painel básico </code> com conteúdo misto e largura fixa. Os painéis não têm largura 
                    fixa para iniciar, portanto, eles preencherão naturalmente toda a largura do elemento <code> Pai</code>. 
                    </p>

                    <!-- Panel -->
                    <div class="row justify-content-center">
                        <div class="col-md-3">
                           <?= $panel ?> 
                        </div>
                    </div>
                    <br/>

                    <!-- Aqui é para ter um exemplo de código --> 

                    <hr>

                    <a name="P2"></a>
                    <h2 class='h2-responsive p-0.5 m-0.5 blue-gradient text-white text-center'>
                        Títulos, Texto, e Links
                    </h2>

                    <p>
                    Os títulos do Painel são adicionados através do <code> .card-title </code> a uma tag <code> &lt;h*&gt;</code>. 
                    Da mesma forma, os links são adicionados através do <code> .card-link </code> em uma tag <code> &lt;a&gt;</code>.                     
                    </p>

                    <div class="row justify-content-center">
                        <div class="col-md-3">
                            <?= $panel2 ?>
                        </div>
                    </div>
                    <br/>

                    <!-- Aqui é para ter um exemplo de código --> 

                    <hr>

                    <a name="P3"></a>
                    <h2 class='h2-responsive p-0.5 m-0.5 blue-gradient text-white text-center'>
                        Cabeçalho e Rodapé
                    </h2>

                    <p>
                    É possível adicionar um cabeçalho ou um rodapé em um painel.
                    </p>

                    <div class="row justify-content-center">
                        <?= $panel3 ?>
                    </div>
                    <br/>

                    <!-- Aqui é para ter um exemplo de código -->

                    <p>
                    Os cabeçalhos do painel podem ser estilizados adicionando <code> .card-header </code> aos
                    elementos <code>&lt;h*&gt;</code>.
                    </p>

                    <br/>

                    <!-- Aqui é para ter um exemplo de código -->

                    <br/>

                    <div class="row justify-content-center">
                        <?= $panel4?>
                    </div>

                    <!-- Aqui é para ter um exemplo de código -->

                    <hr>

                    <a name="P4"></a>
                    <h2 class='h2-responsive p-0.5 m-0.5 blue-gradient text-white text-center'>
                        Usando CSS personalizado
                    </h2>

                    <p>
                    CSS personalizado pode ser usado em suas folhas de estilo ou como estilos embutidos
                    para definir uma largura ou altura. 
                    </p>

                    <div class="justify-content-center">
                        <div class="card" style="width: 20rem;">
                            <?= $panel?>
                        </div>
                    </div>
                    <br/>

                    <!-- Aqui é para ter um exemplo de código -->

                    <hr>

                    <a name="P5"></a>
                    <h2 class='h2-responsive p-0.5 m-0.5 blue-gradient text-white text-center'>
                        Alinhamento de Texto
                    </h2>

                    <p>
                    Podemos alterar rapidamente o alinhamento do texto de qualquer Painel - em sua 
                    totalidade ou em partes específicas - com as <code> classes de alinhamento de texto</code>.  
                    </p>

                    <div class="justify-content-center">
                        <div class="card mb-3" style="width: 20rem;">
                            <?= $panel5 ?>
                        </div>

                        <div class="card text-center mb-3" style="width: 20rem;">
                            <?= $panel5 ?>
                        </div>

                        <div class="card text-right" style="width: 20rem;">
                            <?= $panel5 ?>      
                        </div>
                    </div>
                    <br/>

                    <!-- Aqui é para ter um exemplo de código -->

                    <hr>

                    <a name="P6"></a>
                    <h2 class='h2-responsive p-0.5 m-0.5 blue-gradient text-white text-center'>
                        Estilos de Painel
                    </h2>

                    <p>
                    Os painéis incluem várias opções para personalizar seus planos de fundo, bordas e cores.
                    </p>

                    <hr>

                    <h4><b> Plano de Fundo e Cor </b></h4>

                    <p>
                    Use os <code> utilitários de texto e fundo </code> para alterar a aparência de um Painel.
                    </p>

                    <div class="justify-content-center">
                        <div class="col-mb-3 white-text" style="max-width: 20rem;">
                            <?= $panel6 ?>
                        </div>
                        <br/>

                        <div class="col-mb-3 text-white" style="max-width: 20rem;">
                            <?= $panel7 ?>
                        </div>
                        <br/>

                        <div class="col-mb-3 text-white" style="max-width: 20rem;">
                            <?= $panel8 ?>
                        </div>
                        <br/>

                        <div class="col-mb-3 text-white" style="max-width: 20rem;">
                            <?= $panel9 ?>
                        </div>
                    </div>
                    <br/>

                    <!-- Aqui é para ter um exemplo de código -->

                    <hr>

                    <a name="P7"></a>
                    <h2 class='h2-responsive p-0.5 m-0.5 blue-gradient text-white text-center'>
                        Bordas
                    </h2>

                    <p>
                    Use os <code> utilitários de borda </code> para alterar apenas a <code> cor da borda </code> do Painel. 
                    Observe que você pode colocar as classes <code> .text-{color} </code> no <code> .card </code> Pai ou em  
                    um subconjunto do conteúdo do painel.
                    </p>

                    <div class="justify-content-center">
                        <div class="col-mb-3" style="max-width: 20rem;">
                            <?= $panel10 ?>                            
                        </div>
                        <br/>

                        <div class="col-mb-3" style="max-width: 20rem;">
                            <?= $panel11 ?> 
                        </div>
                        <br/>
                        
                        <div class="col-mb-3" style="max-width: 20rem;">
                            <?= $panel12 ?> 
                        </div>        
                    </div>
                    <br/>

                    <!-- Aqui é para ter um exemplo de código -->

                    <hr>

                <a href="#"><img src="<?= base_url("assets/Img/topo.png") ?>" width="60px" height="80px" title="Topo"></a> 
            </div>
        </div>  
    </div>
</div>