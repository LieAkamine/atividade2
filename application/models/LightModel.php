<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/light/LightLoader.php';

class LightModel extends CI_Model {

    public function getLight() {
        $loader = (object) array(
            "imagem" => 'box2.jpg',
            "alt" => 'Brilho 1',
            "classe" => 'opacity'
        );
        $light = new LightLoader($loader);

        return $light->getHTML();
    }

    public function getLight2() {
        $loader2 = (object) array(
            "imagem" => 'box.jpg',
            "alt" => 'Brilho 2',
            "classe" => 'opacity'
        );
        $light2 = new LightLoader($loader2);

        return $light2->getHTML();
    }

}