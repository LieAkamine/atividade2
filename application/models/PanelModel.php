<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/panel/PanelLoader.php';

class PanelModel extends CI_Model {

    public function getPanel() {
        $loader = (object) array(
            "titulo" => 'Panel title',
            "header" => false,
            "footer" => false,
            "conteudo" => '<p class="card-text"> Some quick example text to build on the panel title and make up the bulk of the panels content. </p>             <a class="card-link"> Card link </a>
            <a class="card-link"> Another link </a>',
            "cor" => '',
            "header_titulo" => '',
            "footer_titulo" => ''
        );
        $panel = new PanelLoader($loader);

        return $panel->getHTML();
    }

    public function getPanel2() {
        $loader2 = (object) array(
            "titulo" => 'Panel title 2',
            "header" => false,
            "footer" => false,
            "conteudo" => '<p class="card-text"> Some quick example text to build on the panel title and make up the bulk of the panels content. </p>     <a href="#!" class="card-link"> Card link </a>
            <a href="#!" class="card-link"> Another link </a>',
            "cor" => '',
            "header_titulo" => '',
            "footer_titulo" => ''
        );
        $panel2 = new PanelLoader($loader2);

        return $panel2->getHTML();
    }

    public function getPanel3() {
        $loader3 = (object) array(
            "titulo" => 'Panel title 3',
            "header" => true,
            "footer" => false,
            "conteudo" => '<p class="card-text"> With supporting text below as a natural lead-in to additional content. </p>',
            "cor" => '',
            "header_titulo" => 'Featured',
            "footer_titulo" => ''
        );
        $panel3 = new PanelLoader($loader3);

        return $panel3->getHTML();
    }

    public function getPanel4() {
        $loader4 = (object) array(
            "titulo" => 'Panel Footer',
            "header" => true,
            "footer" => true,
            "conteudo" => '<p class="card-text"> With supporting text below as a natural lead-in. </p>',
            "cor" => '',
            "header_titulo" => 'Featured',
            "footer_titulo" => '3 days ago'
        );
        $panel4 = new PanelLoader($loader4);

        return $panel4->getHTML();
    }

    public function getPanel5() {
        $loader5 = (object) array(
            "titulo" => 'Panel title 5',
            "header" => false,
            "footer" => false,
            "conteudo" => '<p class="card-text"> Some quick example text to build on the panel title and make up the bulk of the panels content. </p>',
            "cor" => '',
            "header_titulo" => '',
            "footer_titulo" => ''
        );
        $panel5 = new PanelLoader($loader5);

        return $panel5->getHTML();
    }

    public function getPanel6() {
        $loader6 = (object) array(
            "titulo" => 'Panel title 6',
            "header" => true,
            "footer" => false,
            "conteudo" => '<p class="card-text white-text"> Some quick example text to build on the panel title and make up the bulk of the panels content. </p>',
            "cor" => 'bg-primary',
            "header_titulo" => 'Header',
            "footer_titulo" => ''
        );
        $panel6 = new PanelLoader($loader6);

        return $panel6->getHTML();
    }

    public function getPanel7() {
        $loader7 = (object) array(
            "titulo" => 'Panel title 7',
            "header" => true,
            "footer" => false,
            "conteudo" => '<p class="card-text white-text"> Some quick example text to build on the panel title and make up the bulk of the panels content. </p>',
            "cor" => 'bg-danger',
            "header_titulo" => 'Header',
            "footer_titulo" => ''
        );
        $panel7 = new PanelLoader($loader7);

        return $panel7->getHTML();
    }

    
    public function getPanel8() {
        $loader8 = (object) array(
            "titulo" => 'Panel title 8',
            "header" => true,
            "footer" => false,
            "conteudo" => '<p class="card-text white-text"> Some quick example text to build on the panel title and make up the bulk of the panels content. </p>',
            "cor" => 'bg-dark',
            "header_titulo" => 'Header',
            "footer_titulo" => ''
        );
        $panel8 = new PanelLoader($loader8);

        return $panel8->getHTML();
    }

    public function getPanel9() {
        $loader9 = (object) array(
            "titulo" => 'Panel title 9',
            "header" => true,
            "footer" => false,
            "conteudo" => '<p class="card-text white-text"> Some quick example text to build on the panel title and make up the bulk of the panels content. </p>',
            "cor" => 'bg-secondary',
            "header_titulo" => 'Header',
            "footer_titulo" => ''
        );
        $panel9 = new PanelLoader($loader9);

        return $panel9->getHTML();
    }

    public function getPanel10() {
        $loader10 = (object) array(
            "titulo" => 'Panel title 10',
            "header" => true,
            "footer" => false,
            "conteudo" => '<p class="card-text"> Some quick example text to build on the panel title and make up the bulk of the panels content. </p>',
            "cor" => 'border-primary',
            "header_titulo" => 'Header',
            "footer_titulo" => ''
        );
        $panel10 = new PanelLoader($loader10);

        return $panel10->getHTML();
    }

    public function getPanel11() {
        $loader11 = (object) array(
            "titulo" => 'Panel title 11',
            "header" => true,
            "footer" => false,
            "conteudo" => '<p class="card-text"> Some quick example text to build on the panel title and make up the bulk of the panels content. </p>',
            "cor" => 'border-secondary',
            "header_titulo" => 'Header',
            "footer_titulo" => ''
        );
        $panel11 = new PanelLoader($loader11);

        return $panel11->getHTML();
    }

    public function getPanel12() {
        $loader12 = (object) array(
            "titulo" => 'Panel title 12',
            "header" => true,
            "footer" => false,
            "conteudo" => '<p class="card-text"> Some quick example text to build on the panel title and make up the bulk of the panels content. </p>',
            "cor" => 'border-danger',
            "header_titulo" => 'Header',
            "footer_titulo" => ''
        );
        $panel12 = new PanelLoader($loader12);

        return $panel12->getHTML();
    }

}
