<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once 'LightItem.php';
include_once 'LightData.php';

class LightLoader extends CI_Object{

    private $data;

    function __construct($data){
        $this->data = $data;
    }

    public function getHTML(){
        $html = '';
        $data = new LightData($this->data);
        $item = new LightItem($data);
        $html .= $item->getHTML();
        return $html;
    }
}