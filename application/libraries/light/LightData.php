<?php

/**
 * Classe que auxilia na montagem do efeito de Lightbox
 */
class LightData {
    private $imagem;
    private $alt;
    private $classe;

    /**
     * Fazer a construção dos Lightbox
     * @param array $item Recebe os dados para construção do Lightbox
     */
    function __construct($item){
        $this->imagem = $item->imagem;
        $this->alt = $item->alt;
        $this->classe = $item->classe;
    }

    /**
     * Acessar as informações da classe
     * @return array imagem recebe os dados de uma imagem
     */
    public function imagem(){
        return $this->imagem;
    }

    /**
     * Acessar as informações da etiqueta do Lightbox
     * @return array alt recebe o nome da imagem/classe
     */
    public function alt(){
        return $this->alt;
    }

    /**
     * Acessar as informações de CSS do Lightbox
     * @return array classe recebe o nome da classe CSS que é atriubuído
     */
    public function classe(){
        return $this->classe;
    }
}