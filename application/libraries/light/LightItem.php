<?php 
include_once APPPATH.'libraries/component/Component.php'; 
include_once 'LightData.php';

class LightItem extends Component {
    private $data;

    function __construct(LightData $data) {
        $this->data = $data;
    }

    public function getHTML() {
        $html = '<figure>';
        $html .= '<img src=" '.base_url("assets/Img/" .$this->data->imagem()).' "';
        $html .= ' <alt="'.$this->data->alt().'"';
        $html .= ' class="'.$this->data->classe().'">';
        $html .= '</figure>';
        return $html;
    }
}