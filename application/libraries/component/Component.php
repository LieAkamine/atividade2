<?php

// classes abstratas são criadas quando
// queremos determinar comportamentos para 
// as classes filhas.

abstract class Component {
    
    abstract function getHTML();

}