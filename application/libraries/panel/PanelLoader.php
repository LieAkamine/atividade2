<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once 'PanelItem.php';
include_once 'PanelData.php';

class PanelLoader extends CI_Object{

    private $data;

    function __construct($data){
        $this->data = $data;
    }

    public function getHTML(){
        $html = '';
        $data = new PanelData($this->data);
        $item = new PanelItem($data);
        $html .= $item->getHTML();
        return $html;
    }
}