<?php 
include_once APPPATH.'libraries/component/Component.php'; 
include_once 'PanelData.php';

class PanelItem extends Component {
    private $data;

    function __construct(PanelData $data) {
        $this->data = $data;
    }

    public function getHTML() {
        $html = '<div class="card '.$this->data->cor().'">';
        $html .= $this->data->header() == 1 ? '<div class="card-header">'.$this->data->header_titulo().'</div>' : '';
        $html .= '<div class="card-body">';
        $html .= '<h5 class="card-title"> '.$this->data->titulo().' </h5>';
        $html .= $this->data->conteudo();
        $html .= '</div>';
        $html .= $this->data->footer() == 1 ? '<div class="card-footer text-muted">'.$this->data->footer_titulo().'</div>' : '';
        $html .= '</div>';
        return $html;
    }
}



