<?php

/** 
 * Classe que auxilia na montagem de um Painel
*/
class PanelData {
    private $titulo;
    private $header;
    private $footer;
    private $conteudo;
    private $cor;
    private $header_titulo;
    private $footer_titulo; 

    /** 
     * Fazer a construção de um Painel
     * @param array $item Recebe os dados para construção do Painel
    */
    function __construct($item){
        $this->titulo = $item->titulo;
        $this->header = $item->header;
        $this->footer = $item->footer;
        $this->conteudo = $item->conteudo;
        $this->cor = $item->cor;
        $this->header_titulo = $item->header_titulo;
        $this->footer_titulo = $item->footer_titulo;
    }

    /**
     * Acessar as informações da classe
     * @return array titulo recebe os dados de um titulo do painel
     */
    public function titulo(){
        return $this->titulo;
    }

    /**
     * Acessar as informações da classe
     * @return array header recebe os dados de um cabeçalho do painel
     */
    public function header(){
        return $this->header;
    }

    /**
     * Acessar as informações da classe
     * @return array footer recebe os dados de um footer do painel
     */
    public function footer(){
        return $this->footer;
    }

    /**
     * Acessar as informações da classe
     * @return array conteudo recebe os dados de um conteudo do painel
     */
    public function conteudo(){
        return $this->conteudo;
    } 

    /**
     * Acessar as informações da classe
     * @return array cor recebe os dados de um cor do painel
     */
    public function cor(){
        return $this->cor;
    }

    /**
     * Acessar as informações da classe
     * @return array header_titulo recebe os dados de um header_titulo do painel
     */
    public function header_titulo(){
        return $this->header_titulo;
    }

    /**
     * Acessar as informações da classe
     * @return array footer_titulo recebe os dados de um footer_titulo do painel
     */
    public function footer_titulo(){
        return $this->footer_titulo;
    }
}
