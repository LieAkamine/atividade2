<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once 'FormItem.php';
include_once 'FormData.php';

class FormLoader extends CI_Object{

    private $data;

    function __construct($data){
        $this->data = $data;
    }

    public function getHTML(){
        $html = '';
        $data = new FormData($this->data);
        $item = new FormItem($data);
        $html .= $item->getHTML();
        return $html;
    }
}