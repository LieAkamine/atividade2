<?php 

/** 
 * Classe que auxilia na montagem de um Formulário
*/
class FormData {
    private $titulo;
    private $nome;
    private $tipo;
    private $icone_direito;
    private $icone_esquerdo;
    private $tamanho;

    /**
     * Fazer a construção do Formulário
     * @param array $item Recebe os dados para construção do Formulário
     */
    function __construct($item){
        $this->titulo = $item->titulo;
        $this->nome = $item->nome;
        $this->tipo = $item->tipo;
        $this->icone = $item->icone_direito;
        $this->icone = $item->icone_esquerdo;
        $this->tamanho = $item->tamanho;
    }

    
    public function titulo(){
        return $this->titulo;
    }

    public function nome(){
        return $this->nome;
    }

    public function tipo(){
        return $this->tipo;
    }

    public function icone_direito(){
        return $this->icone_direito;
    }

    public function icone_esquerdo(){
        return $this->icone_esquerdo;
    }

    public function tamanho(){
        return $this->tamanho;
    }

}